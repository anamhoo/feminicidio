# feminicidio
Con motivo del #opendataday se se generó el presente proyecto para procesar datos sobre feminicidio en México.
## Procesar datos gubernamentales
En la carpeta DatosGob podrás encontrar información para descargar los datos de Incidencia Delictiva desde 2015 a escala municipal y una serie de pasos para procesar la base de datos usando R.
## Obteniendo información de redes sociales
En la carpeta DatosRedes podrás encontrar una serie de pasos para descargar tweets que tengan como tema feminicidio, para poderlos filtrar por entidad federativa y municipio.
## Bases de datos alternativas
En la carpeta DatosAlterna podrás encontrar una lista de ligas a bases de datos alternativas a las gubernamentales.

