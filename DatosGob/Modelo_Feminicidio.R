##Leer el archivo de incidencia delictiva

Feminicidio_2015_2019 <- read.csv("Archivo_Tabla_delitos",
skip = 1,
header = FALSE,
sep = ",",
col.names=c("Año","Clave_Ent","Entidad","Cve. Municipio","Municipio","Bien jurídico afectado","Tipo de delito","Subtipo de delito","Modalidad","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"))

###Indicar cuales columnas son numéricas
Feminicidio_2015_2019$Año <- as.numeric(Feminicidio_2015_2019$Año)
Feminicidio_2015_2019$Clave_Ent <- as.numeric(Feminicidio_2015_2019$Clave_Ent)
Feminicidio_2015_2019$Cve..Municipio <- as.numeric(Feminicidio_2015_2019$Cve..Municipio)
Feminicidio_2015_2019$Enero <- as.numeric(Feminicidio_2015_2019$Enero)
Feminicidio_2015_2019$Febrero <- as.numeric(Feminicidio_2015_2019$Febrero)
Feminicidio_2015_2019$Marzo <- as.numeric(Feminicidio_2015_2019$Marzo)
Feminicidio_2015_2019$Abril <- as.numeric(Feminicidio_2015_2019$Abril)
Feminicidio_2015_2019$Mayo <- as.numeric(Feminicidio_2015_2019$Mayo)
Feminicidio_2015_2019$Junio <- as.numeric(Feminicidio_2015_2019$Junio)
Feminicidio_2015_2019$Julio <- as.numeric(Feminicidio_2015_2019$Julio)
Feminicidio_2015_2019$Agosto <- as.numeric(Feminicidio_2015_2019$Agosto)
Feminicidio_2015_2019$Septiembre <- as.numeric(Feminicidio_2015_2019$Septiembre)
Feminicidio_2015_2019$Octubre <- as.numeric(Feminicidio_2015_2019$Octubre)
Feminicidio_2015_2019$Noviembre <- as.numeric(Feminicidio_2015_2019$Noviembre)
Feminicidio_2015_2019$Diciembre <- as.numeric(Feminicidio_2015_2019$Diciembre)

###Generar una columna que suma el total para 12 meses ignorando los meses aun sin valor

Feminicidio_2015_2019$Total = rowSums (Feminicidio_2015_2019[ ,10:21], na.rm=TRUE)

###Seleccionar los datos para NombreEntidad [Clave de la entidad 1]
Ent_numeroentidad <- Feminicidio_2015_2019[Feminicidio_2015_2019$Clave_Ent == numeroentidad,]
###Separar los datos que tienen el subtipo [Feminicidio]
Ent_numeroentidad_Fem <- Ent_numeroentidad[Ent_numeroentidad$Subtipo.de.delito == "Feminicidio",]

###De los datos de Feminicidio para NombreEntidad separar por Año
Ent_numeroentidad_2015 <- Ent_numeroentidad_Fem[Ent_numeroentidad_Fem$Año == 2015,]
Ent_numeroentidad_2016 <- Ent_numeroentidad_Fem[Ent_numeroentidad_Fem$Año == 2016,]
Ent_numeroentidad_2017 <- Ent_numeroentidad_Fem[Ent_numeroentidad_Fem$Año == 2017,]
Ent_numeroentidad_2018 <- Ent_numeroentidad_Fem[Ent_numeroentidad_Fem$Año == 2018,]
Ent_numeroentidad_2019 <- Ent_numeroentidad_Fem[Ent_numeroentidad_Fem$Año == 2019,]
###Para el total y para cada año sumar los datos para cada municipio de las cuatro modalidades de feminicidio
Entnumeroentidad_Fem_Mun <- aggregate(Total ~ Cve..Municipio, Ent_numeroentidad_Fem[Ent_numeroentidad_Fem$Modalidad %in% c("Con arma de fuego","Con arma blanca","Con otro elemento","No especificado"),], sum, na.action = na.omit)
Entnumeroentidad_Fem_Mun2015 <- aggregate(Total ~ Cve..Municipio, Ent_numeroentidad_2015[Ent_numeroentidad_2015$Modalidad %in% c("Con arma de fuego","Con arma blanca","Con otro elemento","No especificado"),], sum, na.action = na.omit)
Entnumeroentidad_Fem_Mun2016 <- aggregate(Total ~ Cve..Municipio, Ent_numeroentidad_2016[Ent_numeroentidad_2016$Modalidad %in% c("Con arma de fuego","Con arma blanca","Con otro elemento","No especificado"),], sum, na.action = na.omit)
Entnumeroentidad_Fem_Mun2017 <- aggregate(Total ~ Cve..Municipio, Ent_numeroentidad_2017[Ent_numeroentidad_2017$Modalidad %in% c("Con arma de fuego","Con arma blanca","Con otro elemento","No especificado"),], sum, na.action = na.omit)
Entnumeroentidad_Fem_Mun2018 <- aggregate(Total ~ Cve..Municipio, Ent_numeroentidad_2018[Ent_numeroentidad_2018$Modalidad %in% c("Con arma de fuego","Con arma blanca","Con otro elemento","No especificado"),], sum, na.action = na.omit)
Entnumeroentidad_Fem_Mun2019 <- aggregate(Total ~ Cve..Municipio, Ent_numeroentidad_2019[Ent_numeroentidad_2019$Modalidad %in% c("Con arma de fuego","Con arma blanca","Con otro elemento","No especificado"),], sum, na.action = na.omit)

##Se escribe el archivo como tabla
write.csv(Entnumeroentidad_Fem_Mun, file = "NombreEntidad/NombreEntidad_FemTotal_Mun.csv")

##Asignar a cada municipio su centroide
###Leer tabla municipios que contiene sus centroides
Centorides_Municipios <- read.table("Archivos_Tabla_Centroides", header = TRUE, sep = ",")
###Indicar cuáles son las columnas numéricas
Centorides_Municipios$CVE_ENT = as.numeric(Centorides_Municipios$CVE_ENT)
Centorides_Municipios$CVEGEO = as.numeric(Centorides_Municipios$CVEGEO)
Centorides_Municipios$CVE_MUN = as.numeric(Centorides_Municipios$CVE_MUN)
Centorides_Municipios$X = as.numeric(Centorides_Municipios$X)
Centorides_Municipios$Y = as.numeric(Centorides_Municipios$Y)
###Separar los municipios para NombreEntidad
Centorides_Mun_NombreEntidad <- Centorides_Municipios[Centorides_Municipios$CVE_ENT == 'numeroentidad',]
###Se genera la columna con los valores geográficos de x,y a partir de comparar las clves municipales en las tablas centroides municipales y la de feminicidios
Entnumeroentidad_Fem_Mun$xx <- Centorides_Mun_NombreEntidad$X[match(Entnumeroentidad_Fem_Mun$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]
Entnumeroentidad_Fem_Mun$yy <- Centorides_Mun_NombreEntidad$Y[match(Entnumeroentidad_Fem_Mun$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]

Entnumeroentidad_Fem_Mun2015$xx <- Centorides_Mun_NombreEntidad$X[match(Entnumeroentidad_Fem_Mun2015$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]
Entnumeroentidad_Fem_Mun2015$yy <- Centorides_Mun_NombreEntidad$Y[match(Entnumeroentidad_Fem_Mun2015$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]

Entnumeroentidad_Fem_Mun2016$xx <- Centorides_Mun_NombreEntidad$X[match(Entnumeroentidad_Fem_Mun2016$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]
Entnumeroentidad_Fem_Mun2016$yy <- Centorides_Mun_NombreEntidad$Y[match(Entnumeroentidad_Fem_Mun2016$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]

Entnumeroentidad_Fem_Mun2017$xx <- Centorides_Mun_NombreEntidad$X[match(Entnumeroentidad_Fem_Mun2017$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]
Entnumeroentidad_Fem_Mun2017$yy <- Centorides_Mun_NombreEntidad$Y[match(Entnumeroentidad_Fem_Mun2017$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]

Entnumeroentidad_Fem_Mun2018$xx <- Centorides_Mun_NombreEntidad$X[match(Entnumeroentidad_Fem_Mun2018$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]
Entnumeroentidad_Fem_Mun2018$yy <- Centorides_Mun_NombreEntidad$Y[match(Entnumeroentidad_Fem_Mun2018$Cve..Municipio, Centorides_Mun_NombreEntidad$CVEGEO)]

##Evitar valores na para los datos geográficos
Entnumeroentidad_Fem_Mun <- Entnumeroentidad_Fem_Mun[!is.na(Entnumeroentidad_Fem_Mun$xx),]
Entnumeroentidad_Fem_Mun2015 <- Entnumeroentidad_Fem_Mun2015[!is.na(Entnumeroentidad_Fem_Mun2015$xx),]
Entnumeroentidad_Fem_Mun2016 <- Entnumeroentidad_Fem_Mun2016[!is.na(Entnumeroentidad_Fem_Mun2016$xx),]
Entnumeroentidad_Fem_Mun2017 <- Entnumeroentidad_Fem_Mun2017[!is.na(Entnumeroentidad_Fem_Mun2017$xx),]
Entnumeroentidad_Fem_Mun2018 <- Entnumeroentidad_Fem_Mun2018[!is.na(Entnumeroentidad_Fem_Mun2018$xx),]


###Importar librería rgdal para crear datos geográficos
library("rgdal")
###Asignar como coordenadas a las columnas xx,yy
coordinates(Entnumeroentidad_Fem_Mun) = c("xx", "yy")
coordinates(Entnumeroentidad_Fem_Mun2015) = c("xx", "yy")
coordinates(Entnumeroentidad_Fem_Mun2016) = c("xx", "yy")
coordinates(Entnumeroentidad_Fem_Mun2017) = c("xx", "yy")
coordinates(Entnumeroentidad_Fem_Mun2018) = c("xx", "yy")
##Evitar valores na para los datos geográficos
Entnumeroentidad_Fem_Mun <- Entnumeroentidad_Fem_Mun[!Entnumeroentidad_Fem_Mun$Total < 1,]
Entnumeroentidad_Fem_Mun2015 <- Entnumeroentidad_Fem_Mun2015[!Entnumeroentidad_Fem_Mun2015$Total < 1,]
Entnumeroentidad_Fem_Mun2016 <- Entnumeroentidad_Fem_Mun2016[!Entnumeroentidad_Fem_Mun2016$Total < 1,]
Entnumeroentidad_Fem_Mun2017 <- Entnumeroentidad_Fem_Mun2017[!Entnumeroentidad_Fem_Mun2017$Total < 1,]
Entnumeroentidad_Fem_Mun2018 <- Entnumeroentidad_Fem_Mun2018[!Entnumeroentidad_Fem_Mun2018$Total < 1,]

##Escribir un archivo gjson

writeOGR(Entnumeroentidad_Fem_Mun, dsn="NombreEntidad/NombreEntidad_FemTotal_Mun.GeoJSON", layer="Entnumeroentidad_Fem_Mun", driver="GeoJSON")
writeOGR(Entnumeroentidad_Fem_Mun2015, dsn="NombreEntidad/NombreEntidad_Fem2015_Mun.GeoJSON", layer="Entnumeroentidad_Fem_Mun", driver="GeoJSON")
writeOGR(Entnumeroentidad_Fem_Mun2016, dsn="NombreEntidad/NombreEntidad_Fem2016_Mun.GeoJSON", layer="Entnumeroentidad_Fem_Mun", driver="GeoJSON")
writeOGR(Entnumeroentidad_Fem_Mun2017, dsn="NombreEntidad/NombreEntidad_Fem2017_Mun.GeoJSON", layer="Entnumeroentidad_Fem_Mun", driver="GeoJSON")
writeOGR(Entnumeroentidad_Fem_Mun2018, dsn="NombreEntidad/NombreEntidad_Fem2018_Mun.GeoJSON", layer="Entnumeroentidad_Fem_Mun", driver="GeoJSON")
