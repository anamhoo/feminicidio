# Procesar datos de incidencia delictiva

El Secretariado Ejecutivo del Sistema Nacional de Seguridad publica las cifras de incidencia delictiva actualizadas cada dos meses. En ésta base de datos se encuentra la infomación sobre el número de feminicidios a escala municipal.

## Descargar datos

Las bases de datos se encuentran disponibles en:
https://www.gob.mx/sesnsp/acciones-y-programas/datos-abiertos-de-incidencia-delictiva?state=published
Para los datos municipales de incidencia delictiva de 2015 a enero de 2019 la liga de descarga es:
http://datosabiertos.segob.gob.mx/DatosAbiertos/SESNSP/IDM_NM

## Pre requerimientos
El script está hecho en [R](https://www.r-project.org/).
Adicionalmente se requiere la librería [rgdal](https://cran.r-project.org/web/packages/rgdal/rgdal.pdf).

## Usar el script
Para procesar los datos es importante modificar la ruta para leer el archivo de datos (línea 3).
El script divide los datos por estado, suma los valores de feminicidios: "Con arma de fuego","Con arma blanca","Con otro elemento","No especificado" y genera una tabla. Después divide los datos en feminicidios totales y por año para cada municipio, asigna el centroide municipal y finalmente genera un archivo geojson.

Para usar el script necesitas:

1) Abrir el archivo Modelo_Feminicidio.R en un editor de texto y modificar:

    En la línea 3 tienes que cambiar "Archivo_Tabla_delitos" por la ruta de la tabla de incidencia delictiva que descargaste

    En la línea 54 "Archivos_Tabla_Centroides" por la ruta de la tabla de los centroides municipales que descargas en éste repositorio.

    Cambiar en todos los sitios donde aparece "numeroentidad" por ejemplo si quieres correr el script para la entidad 1 tendrías que poner el número "1".

    Cambia en donde dice "NombreEntidad" por el nombre de la entidad que analizarás sin espacios.

    Después de hacer los cambios tienes que salvar el archivo.

    Crear una carpeta con el nombre de la entidad sin espacios, tal cual la pusiste en el arhcivo Modelo_Feminicidio.R

2) Correr el script Modelo_Feminicidio.R Es posible leer el script en cualquier editor de texto y copiar línea por línea. También es posible poniendo directamente en R:

 > source('Modelo_Feminicidio.R')

## Ejemplo
A manera de ejemplo si queremos correr el script para Aguascalientes sería:

 > source('Aguascalientes_Feminicidio.R')

Aquí dejamos el ejemplo para Aguascalientes, Puebla, Estado de México y Veracruz.

Puedes ver los resultados en un mapa en:

 https://umap.openstreetmap.co/en/map/feminicidio_mexico_2029#7/19.617/-99.196
