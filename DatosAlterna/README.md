# Datos alternativos
En México existe un gran esfuerzo por tener datos sobre feminicidios. A continuación presentamos los links a los mapas en los que se puede descargar los archivos como kml y también tablas.

Feminicidios México realizado por María Salguero
https://feminicidiosmx.crowdmap.com/
Para descargar:
https://www.google.com/maps/d/viewer?mid=174IjBzP-fl_6wpRHg5pkGSj2egE&ll=22.9523095953723%2C-101.4161826021728&z=6

Feminicidios en Puebla de ODESyR:
https://www.google.com/maps/d/viewer?mid=1ggmdbYsvvK5uWuvL8sdfnz52Ims&ll=19.139154805587765%2C-98.08653300910669&z=11

A manera de nota señalamos que es importante revisar directamente en su fuente original la metodología de colecta de datos para determinar su confiabilidad.
